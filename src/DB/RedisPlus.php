<?php


namespace LiteView\DB;


class RedisPlus
{
    private $redis;

    public function __construct($redis)
    {
        $this->redis = $redis;
    }

    public function __call($method, $args)
    {
        // method_exists($this, $afterMethod)
        // call_user_func_array([$this, $afterMethod], $rs);
        if (!empty($args)) {
            if (cfg('redis.prefix')) {
                $args[0] = cfg('redis.prefix') . $args[0];
            } elseif (cfg('app_name') && cfg('app_env')) {
                $args[0] = cfg('app_name') . ':' . cfg('app_env') . ':' . $args[0];
            }
        }
        return call_user_func_array([$this->redis, $method], $args);
    }
}
