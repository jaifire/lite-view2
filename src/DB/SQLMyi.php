<?php

namespace LiteView\DB;


class SQLMyi
{
    private $channel;
    private $result;

    public function __construct($host, $username, $password, $dbname)
    {
        $this->channel = mysqli_connect($host, $username, $password);
        if (mysqli_connect_errno() > 0) {
            trigger_error("数据库连接失败:" . mysqli_connect_errno() . ',' . mysqli_connect_error(), E_USER_ERROR);
        }
        mysqli_select_db($this->channel, $dbname);
        mysqli_query($this->channel, "set names utf8");
    }

    public function query($sql)
    {
        $this->result = mysqli_query($this->channel, $sql);
        return $this;
    }

    public function lastInsertId()
    {
        return mysqli_insert_id($this->channel);
    }

    public function rowCount()
    {
        return mysqli_affected_rows($this->channel);
    }

    public function one()
    {
        return mysqli_fetch_assoc($this->result);
    }

    public function all()
    {
        $data = [];
        while ($row = mysqli_fetch_assoc($this->result)) {
            $data[] = $row;
        }
        return $data;
    }
}