<?php

/**
 * sql 连接与执行
 * PDO::FETCH_ASSOC       //从结果集中获取以列名为索引的关联数组。
 * PDO::FETCH_NUM         //从结果集中获取一个以列在行中的数值偏移量为索引的值数组。
 * PDO::FETCH_BOTH        //这是默认值，包含上面两种数组。
 * PDO::FETCH_OBJ         //从结果集当前行的记录中获取其属性对应各个列名的一个对象。
 * PDO::FETCH_BOUND       //使用fetch()返回TRUE，并将获取的列值赋给在bindParm()方法中指定的相应变量。
 * PDO::FETCH_LAZY        //创建关联数组和索引数组，以及包含列属性的一个对象，从而可以在这三种接口中任选一种。
 *
 * PDO::query() 执行一条SQL语句，如果通过，则返回一个PDOStatement对象。
 * PDO::exec() 执行一条SQL语句，并返回受影响的行数。此函数建议用来进行新增、修改、删除
 * PDOStatement::execute()函数是用于执行已经预处理过的语句，需要配合prepare()函数使用，成功时返回 TRUE， 或者在失败时返回 FALSE
 */

namespace LiteView\DB;


use Exception;
use PDO;


class SQLPdo
{
    private static $pool;
    private $_pdo;

    private function __construct($cfg)
    {
        $driver = $cfg['driver'] ?? 'mysql';
        $charset = $cfg['charset'] ?? 'utf8mb4';
        $prepares = $cfg['prepares'] ?? false;
        $dsn = "$driver:host={$cfg['host']};port={$cfg['port']};dbname={$cfg['dbname']}";
        $options = [
            // 三种异常模式
            // PDO::ERRMODE_SILENT： 默认模式，不主动报错，需要主动以 $pdo->errorInfo()的形式获取错误信息
            // PDO::ERRMODE_WARNING: 引发 E_WARNING 错误
            // PDO::ERRMODE_EXCEPTION: 主动抛出 exceptions 异常，需要以try{}cath(){}捕获错误信息
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND => "set names $charset",
            PDO::ATTR_EMULATE_PREPARES => $prepares,// 默认为true，本地prepare，execute时发送完整的sql，会把数据库数据由int类型转成string
        ];
        $this->_pdo = new PDO($dsn, $cfg['username'], $cfg['password'], $options);
    }

    public static function db($key = 'mysql')
    {
        if (empty(self::$pool[$key])) {
            self::$pool[$key] = new self(cfg($key));
        }
        return self::$pool[$key];
    }

    // * PDO::query() 执行一条SQL语句，如果通过，则返回一个PDOStatement对象。
    public function query($sql)
    {
        try {
            $stmt = $this->_pdo->query($sql);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            return $stmt;
        } catch (Exception $e) {
            trigger_error($e->getMessage() . ';this sql was : ' . $sql, E_USER_ERROR);
        }
    }

    // * PDO::exec() 执行一条SQL语句，并返回受影响的行数。此函数建议用来进行新增、修改、删除
    public function exec($sql, $insert = false)
    {
        try {
            $cnt = $this->_pdo->exec($sql);
            if ($insert) {
                return $this->_pdo->lastInsertId();
            }
            return $cnt;
        } catch (Exception $e) {
            trigger_error($e->getMessage() . ';this sql was : ' . $sql, E_USER_ERROR);
        }
    }

    // * PDOStatement::execute()函数是用于执行已经预处理过的语句，需要配合prepare()函数使用，成功时返回 TRUE， 或者在失败时返回 FALSE
    public function prepare($sql, $prep)
    {
        try {
            $prepare = $this->_pdo->prepare($sql);
            $prepare->setFetchMode(PDO::FETCH_ASSOC);
            foreach ($prep as $field => $value) {
                if (':' != substr($field, 0, 1)) {
                    $field = intval($field) + 1;
                }
                $prepare->bindValue($field, $value);
            }
            $prepare->execute();
            return $prepare;
        } catch (Exception $e) {
            trigger_error($e->getMessage() . ';this sql was : ' . $sql . '; prepare : ' . json_encode($prep), E_USER_ERROR);
        }
    }

    //事务
    public function transaction($func)
    {
        try {
            $this->_pdo->beginTransaction();
            $rst = $func();
            $this->_pdo->commit();
            return $rst;
        } catch (Exception $e) {
            $this->_pdo->rollBack();
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

    public function begin()
    {
        $this->_pdo->beginTransaction();
    }

    public function commit()
    {
        $this->_pdo->commit();
    }

    public function rollBack()
    {
        $this->_pdo->rollBack();
    }
}
