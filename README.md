# lite-view

## 介绍
PHP mini vc 框架

## 项目状态
项目版本**没有遵循** [语义化版本号](https://semver.org/lang/zh-CN/)

## 安装
`composer require lite-view/framework`

## 本地调试
`php -S 127.0.0.1:8000`

## 目录结构

```
├── DB
│       ├── RedisCli.php      # redis
│       ├── SQLFetch.php      # sql 查询
│       ├── SQLMyi.php        # mysqli 连接
│       ├── SQLPdo.php        # sql 连接
│       └── SQLSuid.php       # sql 语句构造器
├── kernel
│       ├── Lite.php          # CURL
│       ├── Route.php         # 路由管理
│       └── Visitor.php       # 请求管理
├── Treasure
│       ├── Log.php           # 日志
│       └── View.php          # 视图渲染
├── environment.php            # 框架环境初始化
└── functions.php              # 框架提供的通用函数
 ```