<?php


const WORKING_DIR = __FILE__;
require_once __DIR__ . '/vendor/autoload.php';


class a
{
    function x()
    {
        $sql = 'select * from users where `tel` LIKE :name or nickname like :name';
        $prep[':name'] = '%a%';
        $r = \LiteView\DB\SQLPdo::db()->prepare($sql, $prep)->fetchAll();
        $r = \LiteView\DB\SQLPdo::db()->query('select * from users where `tel` LIKE "%a%" or nickname like "%a%"')->fetchAll();
        echo json_encode($r, JSON_NUMERIC_CHECK);
    }

    function y()
    {
        echo $a;
    }

    function z()
    {
        echo 'z';
    }
}

LiteView\Kernel\Route::get('/', function (LiteView\Kernel\Visitor $visitor) {
    LiteView\Treasure\Log::error('xxx');
    //$arr = \LiteView\DB\RedisCli::select(0, false)->keys('*');
    //print_r($arr);
    //    var_dump(domain());
    //$r = \LiteView\DB\RedisCli::select()->keys('*');
    //print_r($r);
    //echo '<br/>';
    //$r = \LiteView\DB\RedisCli::usePrefix()->keys('*');
    //print_r($r);
    var_dump($visitor->currentPath());//
});


LiteView\Kernel\Route::group('group', function () {
    LiteView\Kernel\Route::quick('/quick', 'a');
});


LiteView\Kernel\Route::get('/error', function (LiteView\Kernel\Visitor $visitor) {
    try {
        echo 1 / 0;
        echo $a;
    } catch (Throwable $e) {
        var_dump($e->getMessage());
    }
});

LiteView\Kernel\Route::get('/exception', function (LiteView\Kernel\Visitor $visitor) {
    echo1(1);
});

LiteView\Kernel\Route::get('/a/b/c', function (LiteView\Kernel\Visitor $visitor) {
    //var_dump($visitor->currentUri(['a' => 1]));
    var_dump($visitor->currentPath());
});

// 获取路由
$route = LiteView\Kernel\Route::current_route();
list($action, $middleware) = array_values($route);

// 请求处理
$visitor = new LiteView\Kernel\Visitor();
if (is_callable($action)) {
    $response = $action($visitor);
} else {
    list($class, $action) = explode('@', $action);
    $response = (new $class($visitor))->$action($visitor);
}
echo $response;
